#################
# ASCENSION PATHS
#################

@ap_engineered_evolution_POINTS = 3
@ap_engineered_evolution_COST_MULT = -0.25
ap_engineered_evolution = {
	on_enabled = {
		add_research_option = tech_gene_seed_purification
		hidden_effect = {
			country_event = { id = utopia.2700 }
		}
	}
	possible = {
		custom_tooltip = {
			fail_text = "synthetic_empire_biological_ascension"
			NOT = { has_trait = trait_mechanical }
		}
		custom_tooltip = {
			fail_text = "requires_ascension_perks_1"
			num_ascension_perks > 0
		}
		custom_tooltip = {
			fail_text = "requires_technology_gene_tailoring"
			has_technology = tech_gene_tailoring
		}
	}
	modifier = {
		description = ap_engineered_evolution_modifier_desc
		description_parameters = {
			POINTS = @ap_engineered_evolution_POINTS
			COST_MULT = @ap_engineered_evolution_COST_MULT
		}

		BIOLOGICAL_species_trait_points_add = @ap_engineered_evolution_POINTS

		modify_species_cost_mult = @ap_engineered_evolution_COST_MULT
	}

	potential = {
		host_has_dlc = "Utopia"
		NOT = {
			has_ascension_perk = ap_engineered_evolution
			has_authority = "auth_machine_intelligence"
		}
		is_mechanical_empire = no
	}

	ai_weight = {
		factor = 10
		modifier = {
			factor = 0
			num_owned_planets < 10
		}
	}
}

@ap_evolutionary_mastery_POINTS = 3
@ap_evolutionary_mastery_COST_MULT = -0.25
ap_evolutionary_mastery = {
	on_enabled = {
		add_research_option = tech_genetic_resequencing
		hidden_effect = {
			country_event = { id = utopia.2703 }
		}
	}
	possible = {
		custom_tooltip = {
			fail_text = "synthetic_empire_biological_ascension"
			NOT = { has_trait = trait_mechanical }
		}
		custom_tooltip = {
			fail_text = "requires_ascension_perks_2"
			num_ascension_perks > 1
		}
		custom_tooltip = {
			fail_text = "requires_technology_glandular_acclimation"
			has_technology = tech_glandular_acclimation
		}
		custom_tooltip = {
			fail_text = "requires_ap_engineered_evolution"
			has_ascension_perk = ap_engineered_evolution
		}
		custom_tooltip = {
			fail_text = "Can only have one tier two ascension path perk!"
			NOT = { has_ascension_perk = ap_synthetic_evolution }
		}
		custom_tooltip = {
			fail_text = "Can only have one tier two ascension path perk!"
			NOT = { has_ascension_perk = ap_transcendence }
		}
	}
	modifier = {
		description = ap_evolutionary_mastery_modifier_desc
		description_parameters = {
			POINTS = @ap_evolutionary_mastery_POINTS
			COST_MULT = @ap_evolutionary_mastery_COST_MULT
		}

		BIOLOGICAL_species_trait_points_add = @ap_evolutionary_mastery_POINTS

		modify_species_cost_mult = @ap_evolutionary_mastery_COST_MULT
	}

	potential = {
		host_has_dlc = "Utopia"
		NOT = {
			has_ascension_perk = ap_evolutionary_mastery
			has_authority = "auth_machine_intelligence"
		}
		is_mechanical_empire = no
	}

	ai_weight = {
		factor = 100
	}
}

ap_the_flesh_is_weak = {
	on_enabled = {
		custom_tooltip = "flesh_is_weak_tooltip"
		hidden_effect = {
			country_event = { id = utopia.2500 }
		}
	}
	modifier = {
		planet_pops_robotics_upkeep_mult = -0.10
		planet_pop_assembly_mult = 0.10
	}
	possible = {
		custom_tooltip = {
			fail_text = "synthetic_empire_synthetic_ascension"
			NOT = { has_trait = trait_mechanical }
		}
		custom_tooltip = {
			fail_text = "hive_mind_biological_ascension_only2"
			NOT = { has_authority = auth_hive_mind }
		}
		custom_tooltip = {
			fail_text = "requires_ascension_perks_1"
			num_ascension_perks > 0
		}
		custom_tooltip = {
			fail_text = "requires_technology_droid_workers"
			has_technology = tech_droid_workers
		}
	}

	potential = {
		host_has_dlc = "Utopia"
		NOT = {
			has_ascension_perk = ap_the_flesh_is_weak
			has_authority = "auth_machine_intelligence"
		}
		is_mechanical_empire = no
	}

	ai_weight = {
		factor = 20
		modifier = {
			factor = 0.1
			NOR = {
				has_ethic = ethic_materialist
				has_ethic = ethic_fanatic_materialist
			}
		}
		modifier = {
			factor = 0
			num_owned_planets < 10
		}
	}
}

ap_synthetic_evolution = {
	on_enabled = {
		custom_tooltip = "synthetic_evolution_tooltip"
		hidden_effect = {
			country_event = { id = utopia.2550 }
		}
	}
	modifier = {
		planet_jobs_robotic_produces_mult = 0.10
		ROBOT_species_trait_points_add = 1
		modify_species_cost_mult = -0.50
	}
	possible = {
		custom_tooltip = {
			fail_text = "synthetic_empire_synthetic_ascension"
			NOT = { has_trait = trait_mechanical }
		}
		custom_tooltip = {
			fail_text = "hive_mind_biological_ascension_only2"
			NOT = { has_authority = auth_hive_mind }
		}
		custom_tooltip = {
			fail_text = "requires_ascension_perks_2"
			num_ascension_perks > 1
		}
		custom_tooltip = {
			fail_text = "requires_technology_synthetic_workers"
			has_technology = tech_synthetic_workers
		}
		custom_tooltip = {
			fail_text = "requires_technology_synthetic_leaders"
			has_technology = tech_synthetic_leaders
		}
		custom_tooltip = {
			fail_text = "requires_ap_the_flesh_is_weak"
			has_ascension_perk = ap_the_flesh_is_weak
		}
		custom_tooltip = {
			fail_text = "requires_finished_cybernetics_project"
			NOT = { has_special_project = FLESH_IS_WEAK_PROJECT }
		}
		custom_tooltip = {
			fail_text = "Can only have one tier two ascension path perk!"
			NOT = { has_ascension_perk = ap_evolutionary_mastery }
		}
		custom_tooltip = {
			fail_text = "Can only have one tier two ascension path perk!"
			NOT = { has_ascension_perk = ap_transcendence }
		}
	}

	potential = {
		host_has_dlc = "Utopia"
		NOT = {
			has_ascension_perk = ap_synthetic_evolution
			has_authority = "auth_machine_intelligence"
		}
		is_mechanical_empire = no
	}

	ai_weight = {
		factor = 100
	}
}

ap_mind_over_matter = {
	on_enabled = {
		custom_tooltip = "mind_over_matter_tooltip"
		hidden_effect = {
			country_event = { id = utopia.2600 }
		}
		add_research_option = tech_telepathy
	}
	possible = {
		custom_tooltip = {
			fail_text = "synthetic_empire_psionic_ascension"
			NOT = { has_trait = trait_mechanical }
		}
		custom_tooltip = {
			fail_text = "hive_mind_biological_ascension_only1"
			NOT = { has_authority = auth_hive_mind }
		}
		custom_tooltip = {
			fail_text = "requires_ascension_perks_1"
			num_ascension_perks > 0
		}
		custom_tooltip = {
			fail_text = "requires_technology_psionic_theory"
			has_technology = tech_psionic_theory
		}
	}

	potential = {
		host_has_dlc = "Utopia"
		NOT = {
			has_ascension_perk = ap_mind_over_matter
			has_authority = "auth_machine_intelligence"
		}
		is_mechanical_empire = no
	}

	ai_weight = {
		factor = 20
		modifier = {
			factor = 0.1
			NOR = {
				has_ethic = ethic_spiritualist
				has_ethic = ethic_fanatic_spiritualist
			}
		}
		modifier = {
			factor = 0
			num_owned_planets < 10
		}
	}
}

ap_transcendence = {
	on_enabled = {
		custom_tooltip = "transcendence_tooltip"
		hidden_effect = {
			country_event = { id = utopia.2650 }
		}
	}
	possible = {
		custom_tooltip = {
			fail_text = "synthetic_empire_psionic_ascension"
			NOT = { has_trait = trait_mechanical }
		}
		custom_tooltip = {
			fail_text = "hive_mind_biological_ascension_only1"
			NOT = { has_authority = auth_hive_mind }
		}
		custom_tooltip = {
			fail_text = "requires_ascension_perks_2"
			num_ascension_perks > 1
		}
		custom_tooltip = {
			fail_text = "requires_ap_mind_over_matter"
			has_ascension_perk = ap_mind_over_matter
		}
		custom_tooltip = {
			fail_text = "Can only have one tier two ascension path perk!"
			NOT = { has_ascension_perk = ap_evolutionary_mastery }
		}
		custom_tooltip = {
			fail_text = "Can only have one tier two ascension path perk!"
			NOT = { has_ascension_perk = ap_synthetic_evolution }
		}
	}

	potential = {
		host_has_dlc = "Utopia"
		NOT = {
			has_ascension_perk = ap_transcendence
			has_authority = "auth_machine_intelligence"
		}
		is_mechanical_empire = no
	}

	ai_weight = {
		factor = 100
	}
}

#################
# GENERIC ASCENSION PERKS
#################

#Inactive - joined with ap_mastery_of_nature
ap_world_shaper = {
	possible = {
		custom_tooltip = {
			fail_text = "requires_technology_tech_climate_restoration"
			has_technology = tech_climate_restoration
		}
		custom_tooltip = {
			fail_text = "requires_ascension_perks_1"
			num_ascension_perks > 0
		}
	}

	modifier = {
		terraforming_cost_mult = -0.25
	}

	on_enabled = {
		custom_tooltip = "allow_gaia"
	}

	potential = {
		host_has_dlc = "Not Available"
		NOT = {
			has_ascension_perk = ap_world_shaper
		}
		NOT = { has_authority = auth_hive_mind }
		OR = {
			NOT = { has_authority = auth_machine_intelligence }
			has_valid_civic = civic_machine_servitor
			has_valid_civic = civic_machine_assimilator
		}
	}

	ai_weight = {
		factor = 5
	}
}

# Active - joined by ap_interstellar_dominion
ap_galactic_force_projection = {
	possible = {
		custom_tooltip = {
			fail_text = "requires_ascension_perks_2"
			num_ascension_perks > 1
		}
	}
	modifier = {
		country_naval_cap_add = 80
		country_command_limit_add = 20
		country_starbase_influence_cost_mult = -0.20
		country_claim_influence_cost_mult = -0.20
	}

	potential = {
		NOT = {
			has_ascension_perk = ap_galactic_force_projection
		}
	}

	ai_weight = {
		factor = 10
		modifier = {
			factor = 1.5
			num_owned_planets < 10
		}
		modifier = {
			factor = 1.5
			num_owned_planets < 20
		}
		modifier = {
			factor = 2
			has_ethic = ethic_militarist
		}
		modifier = {
			factor = 3
			has_ethic = ethic_fanatic_militarist
		}
	}
}

# Active - joined by ap_galactic_contender
ap_defender_of_the_galaxy = {
	possible = {
		custom_tooltip = {
			fail_text = "requires_ascension_perks_3"
			num_ascension_perks > 2
		}
	}
	modifier = {
		diplo_weight_mult = 0.2
		description = ap_defender_of_the_galaxy_modifier_desc
		damage_vs_country_type_swarm_mult = 0.5
		damage_vs_country_type_extradimensional_mult = 0.5
		damage_vs_country_type_extradimensional_2_mult = 0.5
		damage_vs_country_type_extradimensional_3_mult = 0.5
		damage_vs_country_type_ai_empire_mult = 0.5
		damage_vs_country_type_gray_goo_mult = 0.5
		damage_vs_country_type_fallen_empire_mult = 0.33
		damage_vs_country_type_awakened_fallen_empire_mult = 0.33
		damage_vs_country_type_gate_builders_mult = 0.33
	}

	on_enabled = {
		custom_tooltip = "ap_defender_of_the_galaxy_effect_opinion"
		# see triggered_opinion_ap_defender_of_the_galaxy
	}

	potential = {
		NOT = {
			has_ascension_perk = ap_defender_of_the_galaxy
		}
	}

	ai_weight = {
		factor = 20
		modifier = {
			factor = 0
			NOR = {
				has_global_flag = galactic_crisis_happened
				has_global_flag = gray_goo_crisis_active
			}
		}
	}
}

#Inactive - joined with ap_galactic_force_projection
ap_interstellar_dominion = {
	modifier = {
		country_starbase_influence_cost_mult = -0.20
		country_claim_influence_cost_mult = -0.20
	}

	potential = {
		host_has_dlc = "Not Available"
		NOT = {
			has_ascension_perk = ap_interstellar_dominion
		}
	}

	ai_weight = {
		factor = 10
		modifier = {
			factor = 2
			has_ethic = ethic_xenophobe
		}
		modifier = {
			factor = 3
			has_ethic = ethic_fanatic_xenophobe
		}
	}
}

#Inactive - joined with ap_eternal_vigilance
ap_grasp_the_void = {
	possible = {
		custom_tooltip = {
			fail_text = "requires_ascension_perks_1"
			num_ascension_perks > 0
		}
	}

	modifier = {
		country_starbase_capacity_add = 5
	}

	potential = {
		host_has_dlc = "Not Available"
		NOT = {
			has_ascension_perk = ap_grasp_the_void
		}
	}

	ai_weight = {
		factor = 10
		modifier = {
			factor = 2
			has_civic = civic_inwards_perfection
		}
		modifier = {
			factor = 1.5
			is_xenophobe = yes
		}
	}
}

#Active - joined by ap_grasp_the_void
ap_eternal_vigilance = {
	possible = {
		custom_tooltip = {
			fail_text = "requires_technology_starbase_4"
			has_technology = tech_starbase_4
		}
	}

	modifier = {
		shipclass_starbase_damage_mult = 0.25
		shipclass_starbase_hull_mult = 0.25
		shipclass_military_station_damage_mult = 0.25
		country_starbase_capacity_add = 5
		starbase_defense_platform_capacity_add = 5
	}

	potential = {
		NOT = {
			has_ascension_perk = ap_eternal_vigilance
		}
	}

	ai_weight = {
		factor = 10
		modifier = {
			factor = 2
			has_civic = civic_inwards_perfection
		}
		modifier = {
			factor = 1.5
			is_xenophobe = yes
		}
	}
}

#Inactive - joined with ap_defender_of_the_galaxy
ap_galactic_contender = {
	possible = {
		custom_tooltip = {
			fail_text = "requires_ascension_perks_3"
			num_ascension_perks > 2
		}
	}
	modifier = {
		diplo_weight_mult = 0.2
		damage_vs_country_type_fallen_empire_mult = 0.33
		damage_vs_country_type_awakened_fallen_empire_mult = 0.33
		damage_vs_country_type_gate_builders_mult = 0.33
	}

	potential = {
		host_has_dlc = "Not Available"
		NOT = {
			has_ascension_perk = ap_galactic_contender
		}
		any_relation = {
			has_communications = root
			OR = {
				is_country_type = fallen_empire
				is_country_type = awakened_fallen_empire
			}
		}
	}

	ai_weight = {
		factor = 10
		modifier = {
			factor = 0
			NOR = {
				any_neighbor_country = {
					is_country_type = awakened_fallen_empire
				}
				any_country = {
					is_country_type = fallen_empire
				}
			}
		}
		modifier = {
			factor = 2
			any_country = {
				is_country_type = awakened_fallen_empire
			}
		}
	}
}

ap_mastery_of_nature = {
	on_enabled = {
		custom_tooltip = "allow_decision_mastery_of_nature"
		custom_tooltip = "describe_decision_mastery_of_nature"
		custom_tooltip = "allow_gaia"
	}
	modifier = {
		deposit_blockers_cost_mult = -0.33
		terraforming_cost_mult = -0.25
	}

	potential = {
		NOT = {
			has_ascension_perk = ap_mastery_of_nature
		}
	}

	ai_weight = {
		factor = 10
		modifier = {
			factor = 0.5
			num_ascension_perks > 0
		}
		modifier = {
			factor = 0.1
			num_ascension_perks > 1
		}
	}
}